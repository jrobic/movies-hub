#!/usr/bin/env sh
set -e

yarn lerna run postinstall
yarn patch-package
