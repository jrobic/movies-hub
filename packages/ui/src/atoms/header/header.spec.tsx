import React from 'react';
import { render } from '@testing-library/react';
import { Header } from './Header';

describe('Atoms > Header', () => {
  it('snapshot', () => {
    const { asFragment } = render(
      <Header>
        <div>content</div>
      </Header>,
    );

    expect(asFragment()).toMatchSnapshot();
  });
});
