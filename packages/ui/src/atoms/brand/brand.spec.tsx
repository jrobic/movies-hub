import React from 'react';
import { render } from '@testing-library/react';
import { Brand } from './Brand';

describe('atoms > Brand', () => {
  it('snapshot - full', () => {
    const { asFragment } = render(<Brand />);

    expect(asFragment()).toMatchSnapshot();
  });

  it('snapshot - short', () => {
    const { asFragment } = render(<Brand variant="short" />);

    expect(asFragment()).toMatchSnapshot();
  });
});
