import { css } from '@emotion/core';

export const container = css`
  font-family: 'Anton';
  font-size: 1em;
  margin-right: 1em;

  > span {
    color: var(--color-chi-gong);
  }
`;
