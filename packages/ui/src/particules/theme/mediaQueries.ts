export const breakPoints = {
  sm: 576,
  md: 768,
  lg: 992,
  xl: 1200,
};

const xlarge = `@media (min-width: ${breakPoints.xl}px)`;
const large = `@media (min-width: ${breakPoints.lg}px)`;
const medium = `@media (min-width: ${breakPoints.md}px)`;
const small = `@media (min-width: ${breakPoints.sm}px)`;

export const mq = {
  xlarge,
  large,
  medium,
  small,
};
