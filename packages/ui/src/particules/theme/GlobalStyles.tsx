import { Global, css } from '@emotion/core';
import React from 'react';
import emotionNormalize from 'emotion-normalize';
import { mq } from './mediaQueries';
import { constants } from './constants';

import 'typeface-anton';
import 'typeface-lato';

export const styles = css`
  ${emotionNormalize}

  html,
  body {
    font-family: 'Lato', Helvetica, Arial, sans-serif;
    font-size: 12px;
    line-height: 1.2;
    padding: 0;
    margin: 0;
    min-height: 100vh;
    /* background-color: var(--background-color); */
    color: var(--text-color);
    user-select: none;
    -webkit-font-smoothing: antialiased;
    box-sizing: border-box;

    ${mq.medium} {
      font-size: 14px;
    }

    ${mq.large} {
      font-size: 16px;
    }
  }

  *,
  *::before,
  *::after {
    box-sizing: inherit;
  }

  :root {
    --background-color: #2d3436;
    --text-color: #ffffff;
    --text-color-1: #e5e5e5;
    --text-color-2: #b3b3b3;
    --color-pink-galour: #ff7675;
    --color-chi-gong: #d63031;

    --header-height-sm: ${constants.headerHeightSm};
    --header-height: ${constants.headerHeight};

    --item-line-color: #e0e0e0;
  }
`;

export const GlobalStyles = (): React.ReactElement => <Global styles={styles} />;
