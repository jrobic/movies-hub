/** @jsx jsx */
import { jsx } from '@emotion/core';
import React from 'react';
import { Brand, BrandProps } from '../../atoms';
import { mq, useMediaQueries } from '../../particules';
import * as styles from './headerBrand.styles';

export type HeaderBrandProps = {
  href?: string;
};

export const HeaderBrand = ({ href = '/' }: HeaderBrandProps): React.ReactElement => {
  const variant = useMediaQueries<BrandProps['variant']>([mq.small], ['full'], 'short');

  return (
    <div css={styles.container}>
      <a href={href}>
        <Brand variant={variant} />
      </a>
    </div>
  );
};
