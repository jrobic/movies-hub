import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { HeaderBrand, HeaderBrandProps } from './HeaderBrand';

export default {
  title: 'Molecules/HeaderBrand',
  component: HeaderBrand,
} as Meta;

const Template: Story<HeaderBrandProps> = (args) => <HeaderBrand {...args} />;

export const Default = Template.bind({});
Default.args = {};
