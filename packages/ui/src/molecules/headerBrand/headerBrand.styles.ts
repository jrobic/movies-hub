import { css } from '@emotion/core';
import { mq } from '../../particules';

export const container = css`
  display: flex;
  align-items: center;
  font-size: 1em;
  height: var(--header-height-sm);

  ${mq.small} {
    font-size: 1em;
  }

  ${mq.large} {
    height: var(--header-height);
    font-size: 1.5em;
  }

  a {
    text-decoration: none;
    color: var(--text-color);
    cursor: pointer;
  }
`;
