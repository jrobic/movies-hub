import { css } from '@emotion/core';
import { em } from 'polished';
import { mq } from '../../particules';

export const collapseButtonContainer = css`
  display: flex;
  align-items: center;

  button {
    color: var(--text-color);
    cursor: pointer;
    border: 0;
    background: transparent;
    outline: 0;

    > svg {
      width: ${em('24px')};
      height: ${em('24px')};
    }
  }

  ${mq.large} {
    height: var(--header-height);
    display: none;
  }
`;

export const collapsedContainer = css`
  display: none;
  flex-basis: 100%;
  flex-grow: 1;
  align-items: center;

  ${mq.large} {
    flex-basis: auto;
    display: flex;
  }

  &.open {
    display: flex;
  }
`;

export const container = css`
  height: 100%;
  display: flex;
  flex-direction: column;
  margin: 0;
  padding: 0;
  list-style: none;
  flex: 1;

  > li {
    display: flex;

    height: calc(var(--header-height-sm) + 1px);
    border-bottom: 1px solid var(--item-line-color);

    ${mq.large} {
      height: var(--header-height);
      border-bottom: 0;
    }
  }

  ${mq.large} {
    flex-direction: row;
    flex: none;
  }
`;

export const link = css`
  position: relative;
  text-decoration: none;
  margin: 0 1rem;
  color: var(--text-color-1);
  display: flex;
  align-items: center;
  font-weight: 400;
  cursor: pointer;
  transition: opacity cubic-bezier(0.4, 0, 0.2, 1) 0.3s;

  :hover {
    color: var(--text-color);
  }

  &.current {
    color: var(--text-color);
    font-weight: 700;
    cursor: default;
  }
`;
