/** @jsx jsx */
import { jsx } from '@emotion/core';
import React from 'react';
import { RequireFields } from '../../types/RequireFields';
import { useMainNavigation } from './useMainNavigation';

export type MainNavigationLinkProps = RequireFields<React.HTMLProps<HTMLAnchorElement>, 'href'>;

export const MainNavigationLink = ({
  children,
  href,
  ...props
}: MainNavigationLinkProps): React.ReactElement => {
  const { getLinkProps } = useMainNavigation();

  return (
    <a href={href} {...getLinkProps(href)} {...props}>
      {children}
    </a>
  );
};
