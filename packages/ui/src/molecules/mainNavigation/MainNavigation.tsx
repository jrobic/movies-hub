/** @jsx jsx */
import { jsx } from '@emotion/core';
import React, { useState } from 'react';
import * as styles from './mainNavigation.styles';
import { MainNavigationLink } from './MainNavigationLink';
import { MainNavigationProvider } from './useMainNavigation';

export type MainNavigationProps = React.PropsWithChildren<{
  currentPath?: string;
  onOpenChange?(open: boolean): void;
}>;

export const MainNavigation = ({
  children,
  currentPath = '',
}: MainNavigationProps): React.ReactElement => {
  const [open, setOpen] = useState(false);

  return (
    <MainNavigationProvider currentPath={currentPath}>
      <div css={styles.collapseButtonContainer}>
        <button type="button" onClick={() => setOpen(!open)} data-testid="main-navigation-menu">
          Browse
        </button>
      </div>

      <div
        css={styles.collapsedContainer}
        className={open ? 'open' : ''}
        data-testid="main-navigation-list"
      >
        <ul css={styles.container}>
          {React.Children.map(children, (child) => (
            <li>{child}</li>
          ))}
        </ul>
      </div>
    </MainNavigationProvider>
  );
};

MainNavigation.Link = MainNavigationLink;
