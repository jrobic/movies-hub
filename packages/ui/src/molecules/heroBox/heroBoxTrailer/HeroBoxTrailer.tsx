/** @jsx jsx */
import { jsx } from '@emotion/core';
import React from 'react';
import { Button } from '../../../atoms';
import { mq, useMediaQueries } from '../../../particules';
import * as styles from './heroBoxTrailer.styles';

export type HeroBoxTrailerProps = {
  watchLink: string;
  onMore(): void;
  onAdd(): void;
  synopsis: string;
  title: string;
};

export const HeroBoxTrailer = ({
  watchLink,
  onMore,
  onAdd,
  synopsis = '',
  title,
}: HeroBoxTrailerProps): React.ReactElement => {
  const isMobile = useMediaQueries<boolean>([mq.small], [false], true);

  return (
    <div css={styles.container}>
      <div css={styles.content}>
        <div css={styles.title}>{title}</div>
        <div css={styles.synopsis}>{synopsis}</div>
        <div css={styles.actions}>
          <Button
            startIcon="add"
            variant="text"
            direction="column"
            color="secondary"
            onClick={onAdd}
            css={styles.actionsAdd}
          >
            Ma list
          </Button>
          <a href={watchLink}>
            <Button startIcon="play">Lecture</Button>
          </a>
          <Button
            startIcon="info"
            variant={isMobile ? 'text' : 'contained'}
            direction={isMobile ? 'column' : 'row'}
            color="secondary"
            onClick={onMore}
          >
            {isMobile ? 'Infos' : "Plus d'infos"}
          </Button>
        </div>
      </div>
    </div>
  );
};
