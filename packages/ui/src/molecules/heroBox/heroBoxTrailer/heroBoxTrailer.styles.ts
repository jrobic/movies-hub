import { css } from '@emotion/core';
import { mq } from '../../../particules';

export const container = css`
  display: flex;
  align-items: flex-end;
  justify-content: center;
  position: absolute;
  right: 0;
  left: 0;
  bottom: 0;
  height: 100%;
  padding-bottom: 1em;
  background-image: linear-gradient(to top, rgba(0, 0, 0, 0.7) 30%, rgba(0, 0, 0, 0) 50%);

  ${mq.small} {
    left: 60px;
    bottom: 10%;
    width: 36%;
    background-image: none;
  }
`;

export const content = css`
  display: flex;
  flex-direction: column;
  > div:not(:first-of-type) {
    margin-top: 2vw;
  }
`;

export const title = css`
  font-size: 2em;
  font-weight: 700;
  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.45);
  text-align: center;

  ${mq.small} {
    font-size: 2.5em;
    text-align: left;
  }
`;

export const synopsis = css`
  font-size: 1.4em;
  text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.45);
  display: none;

  ${mq.small} {
    display: block;
  }
`;

export const actionsAdd = css`
  ${mq.small} {
    display: none;
  }
`;

export const actions = css`
  display: flex;
  flex: 1;
  flex-direction: row;
  justify-content: space-evenly;
  align-items: center;

  ${mq.small} {
    justify-content: flex-start;
    > a,
    > button {
      margin-right: 1em;
      margin-bottom: 1em;
    }
  }
`;
