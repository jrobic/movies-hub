import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { HeroBoxImage, HeroBoxImageProps } from './HeroBoxImage';

const defaultProps = {
  srcSets: [
    {
      source: 'https://image.tmdb.org/t/p/original/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
      media: '(min-width: 1199px)',
    },
    {
      source: 'https://image.tmdb.org/t/p/w1280/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
      media: '(min-width: 779px)',
    },
    {
      source: 'https://image.tmdb.org/t/p/w780/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
      media: '(min-width: 500px)',
    },
    {
      source: 'https://image.tmdb.org/t/p/w500/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
      media: '(max-width: 499px)',
    },
  ],
  source: 'https://image.tmdb.org/t/p/w1280/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
  alt: 'Buzzzz',
};

const renderer = (props?: Partial<HeroBoxImageProps>): RenderResult => {
  const mergeProps = {
    ...defaultProps,
    ...props,
  };

  return render(<HeroBoxImage {...mergeProps} />);
};

describe('molecules > HeroBoxImage', () => {
  describe('snapshot', () => {
    it('default', () => {
      const { asFragment } = renderer();

      expect(asFragment()).toMatchSnapshot();
    });

    it('without srcSets', () => {
      const { asFragment } = renderer({ srcSets: undefined });

      expect(asFragment()).toMatchSnapshot();
    });

    it('without alt', () => {
      const { asFragment } = renderer({ alt: undefined });

      expect(asFragment()).toMatchSnapshot();
    });
  });
});
