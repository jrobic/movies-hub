/** @jsx jsx */
import { jsx } from '@emotion/core';
import React from 'react';
import * as styles from './heroBoxImage.styles';

type MediaSource = {
  source: string;
  media: string;
};

export type HeroBoxImageProps = React.HTMLProps<HTMLDivElement> &
  React.PropsWithChildren<{
    srcSets?: MediaSource[];
    source: string;
    alt?: string;
  }>;

export const HeroBoxImage = ({
  srcSets,
  source,
  alt = '',
  ...htmlProps
}: HeroBoxImageProps): React.ReactElement => {
  return (
    <picture css={styles.container} {...htmlProps}>
      {(srcSets || []).map((src) => (
        <source srcSet={src.source} media={src.media} key={src.media} />
      ))}
      <img srcSet={source} alt={alt} />
    </picture>
  );
};
