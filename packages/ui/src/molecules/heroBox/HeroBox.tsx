/** @jsx jsx */
import { jsx } from '@emotion/core';
import React from 'react';
import * as styles from './heroBox.styles';
import { HeroBoxImage, HeroBoxImageProps } from './heroBoxImage';
import { HeroBoxTrailer, HeroBoxTrailerProps } from './heroBoxTrailer';

export type HeroBoxProps = {
  image: HeroBoxImageProps;
} & HeroBoxTrailerProps;

export const HeroBox = ({
  image,
  title,
  onAdd,
  onMore,
  synopsis,
  watchLink,
}: HeroBoxProps): React.ReactElement => {
  return (
    <div css={styles.container}>
      <HeroBoxImage {...image} />
      <HeroBoxTrailer
        title={title}
        onAdd={onAdd}
        onMore={onMore}
        synopsis={synopsis}
        watchLink={watchLink}
      />
    </div>
  );
};
