import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { HeroBox, HeroBoxProps } from './HeroBox';

const imageProps = {
  srcSets: [
    {
      source: 'https://image.tmdb.org/t/p/original/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
      media: '(min-width: 1199px)',
    },
    {
      source: 'https://image.tmdb.org/t/p/w1280/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
      media: '(min-width: 779px)',
    },
    {
      source: 'https://image.tmdb.org/t/p/w780/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
      media: '(min-width: 500px)',
    },
    {
      // source: 'https://image.tmdb.org/t/p/w500/7G9915LfUQ2lVfwMEEhDsn3kT4B.jpg',
      source: 'https://image.tmdb.org/t/p/w500/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
      media: '(max-width: 499px)',
    },
  ],
  source: 'https://image.tmdb.org/t/p/w1280/9FBwqcd9IRruEDUrTdcaafOMKUq.jpg',
  alt: 'Buzzzz',
};

const renderer = (props?: Partial<HeroBoxProps>): [RenderResult, Record<string, jest.Mock>] => {
  const mocks = {
    onMore: jest.fn(),
    onAdd: jest.fn(),
  };

  const mergeProps = {
    image: { ...imageProps },
    watchLink: '/watch/123',
    title: 'Title',
    synopsis: 'synopsis',
    ...mocks,
    ...props,
  };

  return [render(<HeroBox {...mergeProps} />), mocks];
};

describe('molecules > HeroBox', () => {
  it('Should default render', () => {
    const [{ asFragment }] = renderer();

    expect(asFragment()).toMatchSnapshot();
  });
});
