import React from 'react';
import { render } from '@testing-library/react';
import { mockAllIsIntersecting } from 'react-intersection-observer/test-utils';
import { PinnedHeader } from './PinnedHeader';

describe('Atoms > Header', () => {
  it('Should default render', () => {
    const { asFragment, getByTestId } = render(
      <PinnedHeader>
        <div>content</div>
      </PinnedHeader>,
    );

    mockAllIsIntersecting(true);
    expect(getByTestId('pinned-header-wrapper')).toHaveStyle(
      'background-color: var(--background-color)',
    );
    expect(asFragment()).toMatchSnapshot();
  });

  it('Should render w/ background-color when view has been scroll', () => {
    const { asFragment, getByTestId } = render(
      <PinnedHeader>
        <div>content</div>
      </PinnedHeader>,
    );

    mockAllIsIntersecting(false);
    expect(getByTestId('pinned-header-wrapper')).toHaveStyle(
      'background-color: var(--background-color)',
    );

    expect(asFragment()).toMatchSnapshot();
  });
});
